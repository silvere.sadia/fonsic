<?php

namespace App\Model;

interface MentionObjectInterface
{
    public function setMentionManager(MentionManagerInterface $mentionManager);
    public function getMentionManager();

}
