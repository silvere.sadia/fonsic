<?php

namespace App\Repository;

use App\Entity\MentionManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MentionManager|null find($id, $lockMode = null, $lockVersion = null)
 * @method MentionManager|null findOneBy(array $criteria, array $orderBy = null)
 * @method MentionManager[]    findAll()
 * @method MentionManager[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MentionManagerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MentionManager::class);
    }

    // /**
    //  * @return MentionManager[] Returns an array of MentionManager objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MentionManager
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
