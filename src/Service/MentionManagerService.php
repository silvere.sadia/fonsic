<?php

namespace App\Service;

use App\Entity\Mention;
use App\Model\MentionManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Exception\RuntimeException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MentionManagerService {

    private $em;
    private $session;
    private $security;

    public function setEm(EntityManagerInterface $em){
        $this->em = $em;
    }

    public function setSession(SessionInterface $session){
        $this->session = $session;
    }

    public function setSecurity(TokenStorageInterface $security){
        $this->security = $security;
    }

    public function incrementMention(MentionManagerInterface $mentionManager){
        $user = $this->security->getToken()->getUser();
        // $user = $this->container->get('security.context')->getToken()->getUser();
        // dump($user); die();
        if($user instanceof UserInterface){
            foreach($mentionManager->getMentions() as $mention){
                if($mention->getUser() === $user){
                    $mentionManager->removeMention($mention);
                    $this->em->flush();
                    return;
                }
            }

            $mention = new Mention();
            $mention->setCreated(new \Datetime('now'));
            $mention->setUser($user);
            $this->em->persist($mention);
            $mentionManager->addMention($mention);
            $this->em->flush();
        }
        else{
            throw new RuntimeException(sprintf('Pour faire un like il faut se connecter.' ));
        }
    }
}