<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Entity\CommentManager;
use App\Service\CommentManagerService;
use App\Repository\CommentManagerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Exception\RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommentManagerController extends AbstractController
{
    /**
     * @Route("/comment/manager", name="comment_manager")
     */
    public function index()
    {
        return $this->render('comment_manager/index.html.twig', [
            'controller_name' => 'CommentManagerController',
        ]);
    }

    /**
     * @Route("/comment/manager/add/comment/{id}", name="comment_manager_add_comment", requirements={"id": "[0-9]*"})
     */
    public function addComment(CommentManager $commentManager, CommentManagerService $CommentManagerService, Request $request){
        if($commentManager){
            $comment = new Comment();
            $form = $this->createForm(CommentType::class, $comment);
            $form->handleRequest($request);
            // dump($comment); die();
            if($form->isSubmitted() && $form->isValid()){
                try{
                    $CommentManagerService->addComment($commentManager, $comment);
                }
                catch(RuntimeException $e){
                    throw $e;
                }
            }
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/comment/manager/edit/comment/{id}/{im}", name="comment_manager_edit_comment", requirements={"id": "[0-9]*"})
     */
    public function editComment(Comment $comment, $im, CommentManagerService $CommentManagerService, Request $request, CommentManagerRepository $commentManagerRepository){
        if($comment){
            $form = $this->createForm(CommentType::class, $comment);
            $form->handleRequest($request);
            $session = $this->get('session');

            if(!$session->get('comment')){
                $session->set('comment', $comment);
            }
            else{
                $session->remove('comment');
            }

            // dump($session->get('comment')); die();

            if($form->isSubmitted() && $form->isValid()){
                try{
                    $CommentManagerService->editComment($commentManagerRepository->find($im), $comment);
                }
                catch(RuntimeException $e){
                    throw $e;
                }
            }
        }
        
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/comment/manager/delete/comment/{id}/{im}", name="comment_manager_delete_comment", requirements={"id": "[0-9]*", "im": "[0-9]*"}, methods={"DELETE"})
     */
    public function deleteComment(Comment $comment, $im, CommentManagerService $CommentManagerService, Request $request, CommentManagerRepository $commentManagerRepository){
        if($comment){
            if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->get('_token'))) {
                try {
                    $CommentManagerService->removeComment($commentManagerRepository->find($im), $comment);
                } catch (RuntimeException $e) {
                    throw $e;
                }
            }
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/comment/manager/add/comment/{id}", name="comment_manager_add_comment", requirements={"id": "[0-9]*"})
     */
    // public function addResponse(Comment $comment, CommentManagerService $CommentManagerService, Request $request){
    //     if($comment){
    //         $response = new Comment();
    //         $form = $this->createForm(CommentType::class, $response);
    //         $form->handleRequest($request);
    //         // dump($comment); die();
    //         if($form->isSubmitted() && $form->isValid()){
    //             $comment->addResponse($response);
    //         }
    //     }

    //     return $this->redirect($request->headers->get('referer'));
    // }   
}
