<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommentController extends AbstractController
{
    /**
     * @Route("/comment", name="comment")
     */
    public function index()
    {
        return $this->render('comment/index.html.twig', [
            'controller_name' => 'CommentController',
        ]);
    }

    /**
     * @Route("/comment/edit/{id}", name="comment_edit", requirements={"id": "[0-9]*"})
     */
    public function edit(Comment $comment, Request $request)
    {
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        $session = $this->get('session');
        if(!$session->get('precedent')){
            $session->set('precedent', $request->headers->get('referer'));
        }
        
        if($form->isSubmitted() && $form->isValid()){
            $user = $this->getUser();
            if($user instanceof UserInterface){
                if($user === $comment->getUser()){
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                }
                else{
                    throw new RuntimeException(sprintf('Vous n\'avez pas le droit pour modifer ce commentaire.' ));
                }
            }
            else{
                throw new RuntimeException(sprintf('Pour modifer un commentaire il faut se connecter.' ));
            }

            $precedenteRoute = $session->get('precedent');
            $session->remove('precedent');
            return $this->redirect($precedenteRoute);
        }

        return $this->render('front/comment/edit.html.twig', [
            'controller_name' => 'CommentController',
            'form' => $form->createView(),
            'comment'=>$comment,
        ]);
    }
}
