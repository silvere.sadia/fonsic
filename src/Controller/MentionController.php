<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Entity\MentionManager;
use App\Service\MentionManagerService;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/mention")
 */
class MentionController extends AbstractController
{

    /**
     * @Route("/make/{id}", name="mention_make", requirements={"id": "[0-9]*"}, methods={"GET"})
     */
    public function make(MentionManager $mentionManager, MentionManagerService $mentionManagerService, Request $request): Response
    {

        if($mentionManager){
            try{
                $mentionManagerService->incrementMention($mentionManager);
            }
            catch(RuntimeException $e){
                throw $e;
            }

        }
        return $this->redirect($request->headers->get('referer'));
    }
}
