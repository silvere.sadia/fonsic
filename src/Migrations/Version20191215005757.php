<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191215005757 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE comment_manager (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article ADD comment_manager_id INT DEFAULT NULL, CHANGE mention_manager_id mention_manager_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66FADFBE43 FOREIGN KEY (comment_manager_id) REFERENCES comment_manager (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_23A0E66FADFBE43 ON article (comment_manager_id)');
        $this->addSql('ALTER TABLE category CHANGE libele libele VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE pays_id pays_id INT DEFAULT NULL, CHANGE avatar_id avatar_id INT DEFAULT NULL, CHANGE member_id member_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C7294869C');
        $this->addSql('DROP INDEX IDX_9474526C7294869C ON comment');
        $this->addSql('ALTER TABLE comment ADD comment_manager_id INT DEFAULT NULL, DROP article_id');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CFADFBE43 FOREIGN KEY (comment_manager_id) REFERENCES comment_manager (id)');
        $this->addSql('CREATE INDEX IDX_9474526CFADFBE43 ON comment (comment_manager_id)');
        $this->addSql('ALTER TABLE groupes CHANGE name name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE medias CHANGE gallery_id gallery_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE member CHANGE adresse adresse VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE mention CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE status CHANGE created created DATETIME DEFAULT NULL, CHANGE modified modified DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66FADFBE43');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CFADFBE43');
        $this->addSql('DROP TABLE comment_manager');
        $this->addSql('DROP INDEX UNIQ_23A0E66FADFBE43 ON article');
        $this->addSql('ALTER TABLE article DROP comment_manager_id, CHANGE mention_manager_id mention_manager_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE category CHANGE libele libele VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('DROP INDEX IDX_9474526CFADFBE43 ON comment');
        $this->addSql('ALTER TABLE comment ADD article_id INT NOT NULL, DROP comment_manager_id');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_9474526C7294869C ON comment (article_id)');
        $this->addSql('ALTER TABLE groupes CHANGE name name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE medias CHANGE gallery_id gallery_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE member CHANGE adresse adresse VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE mention CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE status CHANGE created created DATETIME DEFAULT \'NULL\', CHANGE modified modified DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE member_id member_id INT DEFAULT NULL, CHANGE pays_id pays_id INT DEFAULT NULL, CHANGE avatar_id avatar_id INT DEFAULT NULL');
    }
}
