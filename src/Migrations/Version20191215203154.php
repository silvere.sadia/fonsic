<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191215203154 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category CHANGE libele libele VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE article CHANGE mention_manager_id mention_manager_id INT DEFAULT NULL, CHANGE comment_manager_id comment_manager_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE pays_id pays_id INT DEFAULT NULL, CHANGE avatar_id avatar_id INT DEFAULT NULL, CHANGE member_id member_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE comment ADD mention_manager_id INT DEFAULT NULL, CHANGE comment_manager_id comment_manager_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CDB4A0274 FOREIGN KEY (mention_manager_id) REFERENCES mention_manager (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9474526CDB4A0274 ON comment (mention_manager_id)');
        $this->addSql('ALTER TABLE groupes CHANGE name name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE medias CHANGE gallery_id gallery_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE member CHANGE adresse adresse VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE mention CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE status CHANGE created created DATETIME DEFAULT NULL, CHANGE modified modified DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE article CHANGE mention_manager_id mention_manager_id INT DEFAULT NULL, CHANGE comment_manager_id comment_manager_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE category CHANGE libele libele VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CDB4A0274');
        $this->addSql('DROP INDEX UNIQ_9474526CDB4A0274 ON comment');
        $this->addSql('ALTER TABLE comment DROP mention_manager_id, CHANGE comment_manager_id comment_manager_id INT DEFAULT NULL, CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE groupes CHANGE name name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE medias CHANGE gallery_id gallery_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE member CHANGE adresse adresse VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE mention CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE status CHANGE created created DATETIME DEFAULT \'NULL\', CHANGE modified modified DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE user CHANGE admin_id admin_id INT DEFAULT NULL, CHANGE member_id member_id INT DEFAULT NULL, CHANGE pays_id pays_id INT DEFAULT NULL, CHANGE avatar_id avatar_id INT DEFAULT NULL');
    }
}
